# User Directory
> 20/02/2020 – INTERVIEW ASSIGNMENT - VARIANT B

You have just joined a large company and have been tasked to create a user directory for the team. The team would like to see a full list of users with unique IDs, create new users, and update a user's profile. This app will primarily be used on mobile but should still work and look good on desktop as well. You will have one working day to complete this app. 

 
## Prerequisites 
- React 
- Material UI (or another similar UI library of your choosing)
- Use with the provided API
- Fork this repo
- Choose one environment to fully support: 
    - Chrome / Android 
    - iOS / Safari 

## Part 1 
- Your app should have a login page
- The login page should communicate with the API to validate credentials 
- The rest of the app should not be accessible if you are not logged in

## Part 2 
- Your app should have a user list page
- The list of users should be retrieved from the API
- The list must show each user's ID, name, and email. Other fields are optional
- _BONUS_: Add a search bar to filter users by name
- _BONUS_: Add pagination

## Part 3
- Your app should have a "Create User" page
- The list page should have a button to navigate to the Create User Page
- The page should have a form that dislays all required fields in the user model (see API docs)
- The form should validate fields (as per API doc) and provide feedback when an input value is invalid
- The form should make the appropriate API call when submitted with valid data
- _BONUS_: Show a loading/success message when the form is submitted
- _BONUS_: Render this page as a dialog/modal

## Part 4
- Your app should have an "Edit User" page
- This page should be shown when an entry in the user list page is clicked
- The page should have a form that displays all required fields in the user model, but only editable fields modifiable (see API docs)
- The form should make the appropriate API call when submitted with valid data
- _BONUS_: The API call only sends the changed fields

## Part 5
- Submit a merge request back to the original GitLab repo
