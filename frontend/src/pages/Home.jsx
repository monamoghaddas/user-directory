import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import * as actions from '../actions/users'
import uiKit from '../components/ui-kit'
import UserBox from '../components/UserBox'
import styled from 'styled-components'
import { Link, Route, Switch } from 'react-router-dom'
import UserForm from '../components/UserForm'
import Header from '../components/Header'
import { mobileL } from '../components/devices'

const { Layout, Row, Col, Text, Button, Input, Drawer, Skeleton, Loader, Pagination } = uiKit
const { Page, PageHeader } = Layout

const { loadUsers } = actions.loadUsers
const { deleteUser } = actions.deleteUser
const ButtonWrapper = styled.div`
  .addBtn{
    margin-left:20px;
    ${mobileL`
      margin-left:0px;
      width:100%;
      margin-bottom:15px;
      `
    }
  }
`
const UserList = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: end;
  ${mobileL`
    justify-content: center;
    `
  }
`

class Home extends Component {
  state = {
    term: '',
    size: 10,
    current: 1
  }
  componentDidMount() {
    const {
      props: { loadUsers }
    } = this
    loadUsers()
  }

  render() {
    const { usersStore: { loading } } = this.props
    return (
      <>
        <Header />
        <Page>
        <PageHeader>
          <Loader fixed show={loading} />
          <Row type="flex" justify="space-between" align="middle">
            <Col>
              <Row type="flex" justify="start">
                <Col>
                  <Text.H2 style={{ marginTop: 7 }}>Users</Text.H2>
                </Col>
                <Col>
                  <ButtonWrapper>
                    <Button
                      style={{ marginTop: 7 }}
                      to={"/add/"}
                      as={Link}
                      type="flat"
                      background="mmoser"
                      className="addBtn"
                      >
                      Add New User
                    </Button>
                  </ButtonWrapper>
                </Col>
              </Row>
            </Col>
            <Col md={6}>
              <Input.Search
                onSearch={this.handleSearchInput}
                placeholder="Filter Users"
              />
            </Col>
          </Row>
        </PageHeader>
        {this.renderUsers()}
        <Switch>
          <Route
            path={`/add/`}
            exact
            render={({ history }) => (
              <Drawer
                width="100%"
                placement="right"
                onClose={() => this.closeDrawer(history, "../")}
                title={
                  <React.Fragment>
                    <Text.H2>Add New User</Text.H2>
                  </React.Fragment>
                }
                visible
              >
                <UserForm isNew closeDrawer={this.closeDrawer} history={history}/>
              </Drawer>
            )}
          />

          <Route
            path={`/edit/:id/`}
            exact
            render={({ history, match: match2 }) => {
              return (
                <Drawer
                  width="lg"
                  placement="right"
                  onClose={() => this.closeDrawer(history, "../../")}
                  title={
                    <React.Fragment>
                      <Text.H2>Edit User</Text.H2>
                    </React.Fragment>
                  }
                  visible
                >
                  <UserForm id={match2.params.id} closeDrawer={this.closeDrawer} history={history}/>
                </Drawer>
              );
            }}
          />

          <Route component={null} />
        </Switch>
      </Page>
      </>
    )
  }

  renderUsers = () => {
    const { usersStore: { loadingUsers }, history } = this.props;
    const { current } = this.state;
    if (loadingUsers) {
      return (
        <Skeleton
          width={320}
          height={260}
          gap={40}
          active
          infocard
          paragraph={{ rows: 5 }}
        />
      )
    }
    const currentPage = this.getCurrentPage()
    return (
      <UserList>
        {currentPage.result.map(user => (
          <UserBox key={user.id} user={user} history={history} handleDelete={this.handleDelete} />
        ))}
        <div style={{ width: '100%', textAlign: 'center'}}>
          <Pagination
            current={current}
            onChange={this.onPageChange}
            total={currentPage.total}
            showTotal={total => `Total ${currentPage.total} items`}
            showSizeChanger
            onShowSizeChange={this.onShowSizeChange}
          />
        </div>
      </UserList>
    )
  }

  handleDelete = (close, id) => {
    const {
      props: { deleteUser }
    } = this
    deleteUser({id})
    close()
  }

  closeDrawer = (history, route) => {
    history.push(route)
  }

  handleSearchInput = (term) => {
    this.setState({term})
  }

  onShowSizeChange = (current, pageSize) => {
    this.setState({ size: pageSize })
  }

  onPageChange = page => {
    this.setState({
      current: page
    })
  }

  getCurrentPage = () => {
    const { usersStore: { users } } = this.props;
    const { size, current, term } = this.state;
    let result = users;
    let total = users.length
    if (term) {
      result = result.filter(user =>
        user.firstName.toLowerCase().includes(term.toLowerCase()) ||
        user.lastName.toLowerCase().includes(term.toLowerCase())
      )
      total = result.length
    }
    result = result.slice((current - 1) * size, current * size)
    return {result, total}
  }
}

Home.propTypes = {
  usersStore: PropTypes.object.isRequired,
  loadUsers: PropTypes.func.isRequired,
  deleteUser: PropTypes.func.isRequired
}

function mapStateToProps(state) {
  const { usersStore } = state
  return { usersStore }
}

export default connect(mapStateToProps, { loadUsers, deleteUser })(Home)
