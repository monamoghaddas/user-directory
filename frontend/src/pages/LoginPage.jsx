import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Redirect } from 'react-router'
import { connect } from 'react-redux'
import { loadUserLogin } from '../actions/app'
import styled from 'styled-components'
import uiKit from '../components/ui-kit'
import loginBg from '../assets/Background.svg'
import { ReactComponent as Logo } from '../assets/mmoser-v1.2.svg'
import { mobileL } from '../components/devices'

const { Layout, Row, Col, Form, Input, Text, Button, Icon } = uiKit

const LoginContent = styled.div`
  padding:50px 0;
  width: 60%;
  ${mobileL`
    width: 80%;
    `}
`

class LoginPage extends Component {
  state = {
    email: '',
    password: ''
  }

  userLogin = e => {
    e.preventDefault()
    const { email, password } = this.state
    this.props.loadUserLogin({ email, password })
  }

  handleChangeEmail = event => {
    this.setState({ email: event.target.value })
  }

  handleChangePassword = event => {
    this.setState({ password: event.target.value })
  }

  render() {
    const { app } = this.props
    if (app.isAuthenticated) {
      return <Redirect to='/' />
    }
    return (
      <Layout style={{ minHeight: '100vh' }}>
        <Layout.Content
          style={{
            background: `url(${loginBg}) #1D3378`,
            backgroundSize: 'cover',
            position: 'sticky',
            left: 0,
            top: 0
          }}
        ></Layout.Content>
        <Layout.Sider collapsedWidth='100%' width='60%' vCenterChild breakpoint='lg'>
          <LoginContent>
            <Row>
              <Col>
                <Form className='login-form' onSubmit={this.userLogin}>
                  <Logo fill='red' className='logo' />
                  <br />
                  <br />
                  <Form.Item>
                    <Input
                      material
                      placeholder='Email'
                      onChange={this.handleChangeEmail}
                      value={this.state.email}
                    />
                  </Form.Item>
                  <Form.Item>
                    <Input
                      material
                      type='password'
                      placeholder='Password'
                      onChange={this.handleChangePassword}
                      value={this.state.password}
                    />
                  </Form.Item>
                  {app.message ? <Text.P color='volcano'>{app.message}</Text.P> : null}
                  <Form.Item>
                    <Button
                      background='mmoser'
                      as='button'
                      size='lg'
                      type='primary'
                      htmlType='submit'
                      className='login-form-button'
                      style={{ width: '100%' }}
                    >
                      {app.submitting ? (
                        <Icon style={{ fontSize: '10px' }} type='loading' />
                      ) : (
                        'Login'
                      )}
                    </Button>
                  </Form.Item>
                </Form>
              </Col>
            </Row>
          </LoginContent>
        </Layout.Sider>
      </Layout>
    )
  }
}

LoginPage.propTypes = {
  app: PropTypes.object.isRequired,
  loadUserLogin: PropTypes.func.isRequired
}

function mapStateToProps(state) {
  const { app } = state
  return { app }
}

const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(LoginPage)
export default connect(mapStateToProps, { loadUserLogin })(WrappedNormalLoginForm)
