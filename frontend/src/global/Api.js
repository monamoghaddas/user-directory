// API clients to communicate with backend
import { getItem } from '../utils'

const getToken = () => {
  return getItem('token')
}

const callApiEndpoint = (endpoint, options = {}) => {
  let status = 0
  return fetch(endpoint, options)
    .then(res => {
      ;({ status } = res)
      return res.json()
    })
    .then(
      result => {
        if (status !== 200 && status !== 201) {
          return { error: result }
        }
        return { response: result }
      },
      error => {
        // eslint-disable-next-line no-console
        console.log('Looks like there was a problem: \n', error)
        return { error }
      }
    )
}
const baseUrl = 'http://localhost:9001'

const createUser = option => {
  const endpoint = `${baseUrl}/users`
  const options = {
    headers: {
      'Content-Type': 'application/json',
      authorization: `Bearer ${getToken()}`
    },
    method: 'POST',
    body: JSON.stringify(option)
  }
  return callApiEndpoint(endpoint, options)
}

const updateUser = (id, option) => {
  const endpoint = `${baseUrl}/users/${id}`
  const options = {
    headers: {
      'Content-Type': 'application/json',
      authorization: `Bearer ${getToken()}`
    },
    method: 'PATCH',
    body: JSON.stringify(option)
  }
  return callApiEndpoint(endpoint, options)
}

const deleteUser = id => {
  const endpoint = `${baseUrl}/users/${id}`
  const options = {
    headers: {
      authorization: `Bearer ${getToken()}`
    },
    method: 'DELETE'
  }
  return callApiEndpoint(endpoint, options)
}

const users = () => {
  const options = {
    headers: {
      authorization: `Bearer ${getToken()}`
    }
  }
  const endpoint = `${baseUrl}/users`
  return callApiEndpoint(endpoint, options)
}

const user = id => {
  const endpoint = `${baseUrl}/users/${id}`
  const options = {
    headers: {
      authorization: `Bearer ${getToken()}`
    }
  }
  return callApiEndpoint(endpoint, options)
}

const userLogin = option => {
  const options = {
    headers: {
      'Content-Type': 'application/json'
    },
    method: 'POST',
    body: JSON.stringify(option)
  }
  const endpoint = `${baseUrl}/login`
  return callApiEndpoint(endpoint, options)
}

export default {
  users,
  user,
  createUser,
  updateUser,
  deleteUser,
  userLogin
}
