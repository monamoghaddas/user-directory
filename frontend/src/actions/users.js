// all actions for users
import * as actions from '../global/actions'
import action from './util'

export const loadUsers = {
  loadUsers: option => action(actions.USERS_LOAD, option),
  loadUsersRequest: () => action(actions.USERS_LOAD_REQUEST),
  loadUsersSuccess: response => action(actions.USERS_LOAD_SUCCESS, { response }),
  failure: error => action(actions.FAILURE, { error })
}

export const loadUser = {
  loadUser: option => action(actions.USER_LOAD, option),
  loadUserRequest: () => action(actions.USER_LOAD_REQUEST),
  loadUserSuccess: response => action(actions.USER_LOAD_SUCCESS, { response }),
  failure: error => action(actions.FAILURE, { error })
}

export const createUser = {
  createUser: option => action(actions.USER_CREATE, option),
  createUserRequest: () => action(actions.USER_CREATE_REQUEST),
  createUserSuccess: response => action(actions.USER_CREATE_SUCCESS, { response }),
  failure: error => action(actions.FAILURE, { error })
}

export const updateUser = {
  updateUser: option => action(actions.USER_UPDATE, option),
  updateUserRequest: () => action(actions.USER_UPDATE_REQUEST),
  updateUserSuccess: response => action(actions.USER_UPDATE_SUCCESS, { response }),
  failure: error => action(actions.FAILURE, { error })
}

export const deleteUser = {
  deleteUser: option => action(actions.USER_DELETE, option),
  deleteUserRequest: () => action(actions.USER_DELETE_REQUEST),
  deleteUserSuccess: response => action(actions.USER_DELETE_SUCCESS, { response }),
  failure: error => action(actions.FAILURE, { error })
}
