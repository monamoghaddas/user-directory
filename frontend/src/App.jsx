import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { ConnectedRouter } from 'connected-react-router'
import { connect } from 'react-redux'
import Routes from './Routes'
import uiKit from './components/ui-kit'
import 'antd/dist/antd.css'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

const { Theme } = uiKit

class App extends Component {
  render() {
    return (
      <ConnectedRouter history={this.props.history}>
        <ToastContainer autoClose={5000} />
        <Theme.GlobalStyles />
        <Routes />
      </ConnectedRouter>
    )
  }
}

App.propTypes = {
  history: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
  appState: PropTypes.object.isRequired
}

function mapStateToProps(state) {
  return { appState: state }
}

export default connect(mapStateToProps)(App)
