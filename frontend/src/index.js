import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { StoreContext } from 'redux-react-hook'
import App from './App'
import configureStore, { history } from './config/store'
import rootSaga from './sagas'
import './assets/fonts/stylesheet.css'

const { store, runSaga } = configureStore()
runSaga(rootSaga)
ReactDOM.render(
  <Provider store={store}>
    <StoreContext.Provider value={store}>
      <div className='App'>
        <App history={history} />
      </div>
    </StoreContext.Provider>
  </Provider>,
  document.getElementById('root')
)
