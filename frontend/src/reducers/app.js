import * as actions from '../global/actions'
import { setItem, getItem } from '../utils'

const ATTRS = ['token']

function addUserCache(user) {
  ATTRS.forEach(attr => {
    if (user[attr]) setItem(attr, user[attr])
  })
}

function getUserCache() {
  const user = {}
  ATTRS.forEach(attr => {
    user[attr] = getItem[attr]
  })
  return user
}

function delUserCache() {
  ATTRS.forEach(attr => {
    localStorage.removeItem(attr)
  })
}

const initState = {
  isAuthenticated: !!localStorage.token,
  user: getUserCache(),
  message: '',
  submitting: false,
  backendUrl: ''
}

export default function(state = initState, action = {}) {
  switch (action.type) {
    case actions.USER_LOGIN_REQUEST:
      return {
        ...state,
        submitting: true
      }

    case actions.USER_LOGIN_SUCCESS:
      addUserCache(action.response)
      return {
        ...state,
        isAuthenticated: true,
        user: action.response || {},
        submitting: false,
        message: ''
      }

    case actions.USER_LOGIN_FAILURE:
      delUserCache()
      return {
        ...state,
        isAuthenticated: false,
        user: {},
        submitting: false,
        message: action.error ? action.error.message : ''
      }

    case actions.USER_LOGOUT_SUCCESS:
      delUserCache()
      return {
        ...state,
        isAuthenticated: false,
        user: {},
        message: ''
      }

    default:
      return state
  }
}
