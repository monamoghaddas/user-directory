import * as actions from '../global/actions'

const initState = {
  user: null,
  loadingUser: true
}

export default function(state = initState, action = {}) {
  let response
  switch (action.type) {
    case actions.USER_LOAD_REQUEST:
      return {
        ...state,
        loadingUser: true
      }
    case actions.USER_LOAD_SUCCESS:
      ;({ response } = action)
      return {
        ...state,
        user: response,
        loadingUser: false
      }
    default:
      return state
  }
}
