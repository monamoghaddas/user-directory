import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'

import app from './app'
import usersStore from './users'
import userStore from './user'

export default history =>
  combineReducers({
    router: connectRouter(history),
    app,
    usersStore,
    userStore
  })
