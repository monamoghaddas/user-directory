import * as actions from '../global/actions'

const initState = {
  users: [],
  loadingUsers: false,
  loading: false
}

export default function(state = initState, action = {}) {
  let response
  let storeUsers
  switch (action.type) {
    case actions.USERS_LOAD_REQUEST:
      return {
        ...state,
        loadingUsers: true
      }
    case actions.USERS_LOAD_SUCCESS:
      ;({ response } = action)
      return {
        ...state,
        users: response,
        loadingUsers: false
      }
    case actions.USER_CREATE_REQUEST:
      return {
        ...state,
        loading: true
      }
    case actions.USER_CREATE_SUCCESS:
      ;({ response } = action)
      storeUsers = [...state.users]
      storeUsers.push(response)
      return {
        ...state,
        users: storeUsers,
        loading: false
      }
    case actions.USER_DELETE_REQUEST:
      return {
        ...state,
        loading: true
      }
    case actions.USER_DELETE_SUCCESS:
      ;({ response } = action)
      storeUsers = [...state.users]
      storeUsers = storeUsers.filter(user => user.id !== response.id)
      return {
        ...state,
        users: storeUsers,
        loading: false
      }
    case actions.USER_UPDATE_REQUEST:
      return {
        ...state,
        loading: true
      }
    case actions.USER_UPDATE_SUCCESS:
      ;({ response } = action)
      storeUsers = [...state.users]
      storeUsers = storeUsers.map(user => {
        if (user.id === response.id) {
          return response
        }
        return user
      })
      return {
        ...state,
        users: storeUsers,
        loading: false
      }
    default:
      return state
  }
}
