import React, { Component } from 'react'
import uiKit from "./ui-kit";
import styled from "styled-components";
import * as actions from "../actions/users";
import { connect } from "react-redux";

const {
  Text,
  Input,
  Button,
  Form,
  Layout,
  Skeleton
} = uiKit

const Section = styled.div`
  margin-bottom: 5px;
`
const RequiredStar = styled.span`
  color: #fa541c;
`;

const SkeletonLayout = styled.div`
  padding: 100px;
  max-width: 900px;
`;

class UserForm extends Component {
  state = {
    showError: false
  }

  componentDidMount() {
    const { isNew, dispatch, id, form } = this.props;
    if (!isNew) {
      dispatch(actions.loadUser.loadUser({id}));
    }
    form.validateFields();
  }

  render() {
    const { isNew, style, form, loading } = this.props;
    const { showError } = this.state;
    const {
      getFieldDecorator,
      getFieldError,
      isFieldsTouched
    } = form;

    // Only show error after a field is touched.
    const firstNameError = showError && getFieldError("firstName");
    const lastNameError = showError && getFieldError("lastName");
    const emailError = showError && getFieldError("email");
    const passwordError = showError && getFieldError("password");
    const phoneError = showError && getFieldError("phone");

    if (loading) {
      return (
        <Layout>
          <SkeletonLayout>
            <Skeleton active list paragraph={{ rows: 1 }} />
            <div style={{ height: 50 }} />
            <Skeleton active list paragraph={{ rows: 5 }} />
          </SkeletonLayout>
        </Layout>
      );
    }
    return (
      <Form onSubmit={this.handleSubmit}>
        <div style={style}>
          <Form.Item
            validateStatus={firstNameError ? "error" : ""}
            help={firstNameError || ""}
          >
            <Section>
              <Text.H6>
                First Name<RequiredStar>*</RequiredStar>
              </Text.H6>
              {getFieldDecorator("firstName", {
                rules: [{ required: true, message: "Please input first name!" }]
              })(
                <Input placeholder="Enter first name" />
              )}
            </Section>
          </Form.Item>
          <Form.Item
            validateStatus={lastNameError ? "error" : ""}
            help={lastNameError || ""}
          >
            <Section>
              <Text.H6>
                Last Name<RequiredStar>*</RequiredStar>
              </Text.H6>
              {getFieldDecorator("lastName", {
                rules: [{ required: true, message: "Please input last name!" }]
              })(
                <Input placeholder="Enter last name" />
              )}
            </Section>
          </Form.Item>
          <Form.Item
            validateStatus={emailError ? "error" : ""}
            help={emailError || ""}
          >
            <Section>
              <Text.H6>
                Email<RequiredStar>*</RequiredStar>
              </Text.H6>
              {getFieldDecorator("email", {
                rules: [{ required: true, message: "Please input valid email!", type: 'email' }]
              })(
                <Input placeholder="Enter email" />
              )}
            </Section>
          </Form.Item>
          <Form.Item
            validateStatus={passwordError ? "error" : ""}
            help={passwordError || ""}
          >
            <Section>
              <Text.H6>
                Password{isNew? <RequiredStar>*</RequiredStar> : null}
              </Text.H6>
              {getFieldDecorator("password",{
                  ...(isNew && {
                  rules: [{ required: true, message: "Please input password!" }]
                })
                }
                )(
                <Input type='password' placeholder="Enter password" />
              )}
            </Section>
          </Form.Item>
          <Form.Item
            validateStatus={phoneError ? "error" : ""}
            help={phoneError || ""}
          >
            <Section>
              <Text.H6>
                Phone<RequiredStar>*</RequiredStar>
              </Text.H6>
              {getFieldDecorator("phone", {
                rules: [{ required: true, message: "Please input phone!" }]
              })(
                <Input placeholder="Enter phone" />
              )}
            </Section>
          </Form.Item>
          <Form.Item>
            <Section>
              <Text.H6>
                Title
              </Text.H6>
              {getFieldDecorator("title")(
                <Input placeholder="Enter title" />
              )}
            </Section>
          </Form.Item>
          <Button
            background="mmoser"
            as="button"
            htmlType="submit"
            style={{ marginTop: 10, marginBottom: 20 }}
            size="lg"
            type="primary"
            disabled={!isFieldsTouched()}
          >
            {isNew ? "Add" : "Update"}
          </Button>
        </div>
      </Form>
    );
  }

  handelInput = (e, name) => {
    const { form } = this.props;
    form.setFieldsValue({
      [name]: e.target.value
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    const { dispatch, id, isNew, history, closeDrawer, form, user } = this.props;
    form.validateFields((err, values) => {
      if (!err) {
        if (isNew) {
          dispatch(actions.createUser.createUser(values));
        } else {
          const diff = Object.keys(values).reduce((diff, key) => {
            if (user[key] === values[key]) return diff
            return {
              ...diff,
              [key]: values[key]
            }
          }, {})
          dispatch(actions.updateUser.updateUser({values: diff, id}));
        }
        closeDrawer(history,"/");
      } else {
        this.setState({showError: true});
      }
    });
  };
}

const WrappedUserForm = Form.create({
  name: "user_form",
  mapPropsToFields(props) {
    const { user, isNew } = props;
    if (user && !isNew) {
      return {
        firstName: Form.createFormField({ value: user.firstName }),
        lastName: Form.createFormField({ value: user.lastName }),
        phone: Form.createFormField({ value: user.phone }),
        email: Form.createFormField({ value: user.email }),
        title: Form.createFormField({ value: user.title })
      };
    }
    return null;
  }
})(UserForm);

function mapStateToProps(state) {
  const { userStore } = state;
  const { user, loadingUser} = userStore
  return { user, loadingUser};
}

export default connect(mapStateToProps)(WrappedUserForm);
