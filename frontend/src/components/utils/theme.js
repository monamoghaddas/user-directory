export default {
  bg: {
    primary: '#0096FF',
    primaryD1: '#1B65ED',
    primaryD2: '#1B65ED',
    secondary: '#1BB99A',
    secondaryD1: '#00A466',
    secondaryD2: '#0B8900'
  },
  text: {
    primary: '#fff',
    primaryD1: '#fff',
    primaryD2: '#fff',
    secondary: '#fff',
    secondaryD1: '#fff',
    secondaryD2: '#fff'
  },
  borderRadius: '6px'
}
