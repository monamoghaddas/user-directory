export const PRIMARY = '#10BDE5'
export const PRIMARY_D1 = '#0A91ED'
export const PRIMARY_D2 = '#1B65ED'
export const PRIMARY_TEXT = '#FFF'

export const SECONDARY = '#1BB99A'
export const SECONDARY_D1 = '#00A466'
export const SECONDARY_D2 = '#0B8900'
export const SECONDARY_TEXT = '#FFF'

export const BORDER_RADIUS = '6px'
