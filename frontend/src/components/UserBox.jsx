import React from 'react'
import uiKit from '../components/ui-kit'
import { Link } from 'react-router-dom'
import { MdDelete, MdModeEdit } from 'react-icons/md'
import TimeAgo from 'javascript-time-ago'
import en from 'javascript-time-ago/locale/en'
import styled from 'styled-components'

TimeAgo.addLocale(en)
const timeAgo = new TimeAgo('en-US')

const { Layout, Text, Button, Card, Options, Icon } = uiKit

const handleRedirect = (id, history) => {
  return history.push(`/edit/${id}`)
}

const UserBoxContent = styled.div`
  transition: all .2s ease-in-out;
  display: inline-block;
  min-width: 350px;
  text-align: center;
  :hover {
    transform: scale(1.05);
    z-index: 2;
  }
  ${Text}{
    white-space: nowrap;
    text-overflow: ellipsis;
    margin-bottom: 0;
    width: 150px;
    overflow: hidden;
  }
`

const FieldItem = styled.div`
  display: flex;
`

const UserBox = props => {
  const { user, handleDelete, history } = props
  return (
    <UserBoxContent>
      <Card
      key={user.id}
      hoverable
      p0
      className='userBox'
      style={{
        marginBottom: 40,
        display: 'inline-block',
        maxWidth: 320,
        minHeight: 300,
        textAlign: 'left'
      }}
      >
        <Layout style={{ padding: "20px 0" }}>
        <Layout.Content vCenter style={{ marginLeft: 20 }}>
          <div style={{ width: "100%" }}>
            <div onClick={()=>handleRedirect(user.id, history)}>
              <FieldItem>
                <Text.H6 m0 style={{ display: 'inline-block' }}>ID:</Text.H6>
                <Text style={{ display: 'inline-block', marginLeft: 3 }}>{user.id}</Text>
              </FieldItem>
              <FieldItem>
                <Text.H6 m0 style={{ display: 'inline-block' }}>NAME:</Text.H6>
                <Text
                  style={{ display: 'inline-block', marginLeft: 3 }}
                  >{`${user.firstName} ${user.lastName}`}</Text>
              </FieldItem>
              <FieldItem>
                <Text.H6 m0 style={{ display: 'inline-block' }}>EMAIL:</Text.H6>
                <Text style={{ display: 'inline-block', marginLeft: 3 }}>{user.email}</Text>
              </FieldItem>
              <FieldItem>
                <Text.H6 m0 style={{ display: 'inline-block' }}>PHONE:</Text.H6>
                <Text style={{ display: 'inline-block', marginLeft: 3 }}>{user.phone}</Text>
              </FieldItem>
              <FieldItem>
                <Text.H6 m0 style={{ display: 'inline-block' }}>TITLE:</Text.H6>
                <Text style={{ display: 'inline-block', marginLeft: 3 }}>{user.title}</Text>
              </FieldItem>
              <FieldItem>
                <Text.H6 m0 style={{ display: 'inline-block' }}>CREATED AT:</Text.H6>
                <Text style={{ display: 'inline-block', marginLeft: 3 }}>{timeAgo.format(new Date(user.createdAt))}</Text>
              </FieldItem>
              <FieldItem>
                <Text.H6 m0 style={{ display: 'inline-block' }}>UPDATED AT:</Text.H6>
                <Text style={{ display: 'inline-block', marginLeft: 3 }}>{timeAgo.format(new Date(user.updatedAt))}</Text>
              </FieldItem>
            </div>
            <div
              style={{
                position: "absolute",
                left: 15,
                bottom: 15,
                zIndex: 3
              }}
            >
              <Options
                triggerComponent={
                  <Button type="list" size="sm">
                    <Icon type="ellipsis" />
                  </Button>
                }
              >
                {({ close }) => (
                  <div>
                    <Options.Content>
                      <Button
                        background="mmoser"
                        onClick={close}
                        type="list"
                        as={Link}
                        to={`/edit/${user.id}`}
                        style={{ padding: "10px" , width: '100%'}}
                      >
                        <>
                          <MdModeEdit style={{ marginRight: "10px" }} />
                          Edit User
                        </>
                      </Button>
                      <br/>
                      <br/>
                      <Button
                        background="red"
                        onClick={() => handleDelete(close, user.id)}
                        type="list"
                        style={{ padding: "10px", width: '100%' }}
                      >
                        <>
                          <MdDelete style={{ marginRight: "10px" }} />
                          Delete User
                        </>
                      </Button>
                    </Options.Content>
                  </div>
                )}
              </Options>
            </div>
          </div>
        </Layout.Content>
        <Layout.Sider width={35}></Layout.Sider>
      </Layout>
      </Card>
    </UserBoxContent>
  )
}

export default UserBox
