import styled, { css } from 'styled-components'

import { Card as AntCard } from 'antd'
import PropTypes from 'prop-types' // eslint-disable-line no-unused-vars
import React from 'react'
import theme from '../../utils/theme'

const colors = {
  gold: 'rgb(250,173,20)',
  red: 'rgb(230,41,41)',
  green: 'rgb(82,196,26)',
  magenta: 'rgb(235,47,150)',
  volcano: 'rgb(250,84,28)',
  blue: 'rgb(0,150,255)',
  geekblue: 'rgb(27, 101, 237)',
  orange: 'rgb(250,140,22)',
  lime: 'rgb(160,217,17)',
  cyan: 'rgb(27,185,154)',
  purple: 'rgb(114,46,209)'
}

// const Card = ({ flat, p0, ...props }) => (<AntCard {...props} />);
const Card = React.forwardRef((props, ref) => {
  // eslint-disable-next-line no-unused-vars
  const { flat, p0, ...rest } = props
  return <AntCard ref={ref} {...rest} />
})
Card.propTypes = {
  flat: PropTypes.bool,
  p0: PropTypes.bool
}

/*
  If background is passed as per the above colors then override the background color
  and also set color to white.
*/
export default styled(Card)`
  &&&&{
    border-radius: ${theme.borderRadius};  
    border: none;
    width: 100%;
    transition: none;

    /* Emulate border using box shadow for crisp border color on any background color */
    box-shadow: 0 0 0 1px rgba(0,0,0,0.05), 0px 3px 8px rgba(0,0,0,0.05);

    ${props =>
      props.stretch &&
      css`
        height: 100%;
      `}

    ${props =>
      props.background &&
      css`
        ${colors[props.background]
          ? css`
              background-color: ${colors[props.background]};
              color: #fff;
              & {
                h1,
                h2,
                h3,
                h4,
                h5,
                h6 {
                  color: #fff;
                }
              }
            `
          : css`
              background-color: ${props.background};
            `}
      `}

    ${props =>
      props.color &&
      css`
        color: ${props.color};
        & {
          h1,
          h2,
          h3,
          h4,
          h5,
          h6 {
            color: ${props.color};
          }
        }
      `}

    /* 
      If background is specified on a flat button, then no shadow 
      But if the card is white, flat Card will have a default border
    */
    ${props =>
      props.flat &&
      css`
        ${props.background &&
          css`
            box-shadow: none;
          `}
        ${!props.background &&
          css`
            box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.08);
          `}
      `}

    /**
    *  Zero padding card   
    */
    ${props =>
      props.p0 &&
      css`
        & > .ant-card-body {
          padding: 0;
        }
      `}

    /**
    * Elevations
    */
    ${props =>
      props.elevation &&
      css`
      ${props.elevation === 0 &&
        css`
          box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.05);
        `}

      ${props.elevation === 1 &&
        css`
          box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.05), 0px 3px 8px rgba(0, 0, 0, 0.05);
        `}

      ${props.elevation === 2 &&
        css`
          box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.05), 0px 5px 12px rgba(0, 0, 0, 0.08);
        `}
    `}
  }
`
