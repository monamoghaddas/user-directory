import { Card as AntCard } from 'antd'
import Card from './Card'
import InfoCard from './info-card'

export default Card

Card.InfoCard = InfoCard
Card.Meta = AntCard.Meta
