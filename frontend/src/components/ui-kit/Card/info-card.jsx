import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import CountUp from 'react-countup'
import Card from './Card'
import Text from '../Text'

const getValueParts = val => {
  if (typeof val === 'undefined') {
    return ['-']
  }

  // Cast number to string and split via numeric
  return `${val}`.split(/(\d+)/)
}

const formatNumber = (actualNumber, animatedVal) => {
  const newAnimVal = Number(animatedVal).toLocaleString('EN-us')
  return `${newAnimVal}`.padStart(`${actualNumber}`.length, '0')
}

const StyledCountUp = styled(CountUp)`
  position: absolute;
  left: 0;
  top: 0;
`

const NumberWrap = styled.div`
  position: relative;
  display: inline-block;
`

const Value = styled(Text.H1)`
  position: relative;
`

const InfoCard = ({ title, ...props }) => (
  <Card {...props} flat>
    <Text.P className='title'>{title}</Text.P>
    <Value className='value'>
      {getValueParts(props.value).map((val, i) => {
        const num = parseFloat(val)
        if (isNaN(num)) {
          return val
        }
        return (
          <React.Fragment key={i}>
            <NumberWrap>
              <span style={{ visibility: 'hidden' }}>
                {Number(val).toLocaleString('EN-us')}
              </span>
              <StyledCountUp
                formattingFn={val => formatNumber(num, val)}
                easing={false}
                duration={1}
                end={num}
              />
            </NumberWrap>
          </React.Fragment>
        )
      })}
    </Value>

    {props.children}
  </Card>
)

InfoCard.propTypes = {
  children: PropTypes.node,
  title: PropTypes.node,
  value: PropTypes.node
}

export default styled(InfoCard)`
  &&&& {
    display: inline-block;
    width: auto;
    padding: 12px;
    margin-right: 10px;
    max-width: 300px;

    .ant-card-body {
      padding: 12px;
    }

    & * {
      color: inherit;
    }

    .value {
      margin: 0;
      line-height: 30px;
      font-size: 270%;
      font-weight: 300;
    }
    .title {
      line-height: 15px;
      margin-bottom: 5px;
      font-size: 100%;
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
    }
  }
`
