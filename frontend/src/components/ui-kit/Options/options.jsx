import React from 'react';
import PropTypes from 'prop-types';
import styled, {css} from 'styled-components';
import { MdMoreVert } from 'react-icons/md';
import Button from '../Button';
import Card from '../Card';
import Separator from '../Layout/seperator';
import { useSpring, animated } from 'react-spring';

const OptionsWrap = styled.div`
  position: relative;
  display: inline-block;
`;

const AnimatedCard = styled(animated(Card))`
  transform-origin: top left;
  transition: none;
`;

const ContainerWrap = styled.div`
  position: absolute;
  left: 0;
  bottom: 40px;
  z-index: 1;

  ${props => props.fixed && css`
      position: fixed;
      max-width: 240px;
      z-index: 5;
  `}

  ${props => props.fixedStyle && css`

    top: auto;
    left: auto;

    ${props.fixedStyle.top && css`
      top: ${props.fixedStyle.top}px;
    `}
    ${props.fixedStyle.bottom && css`
      bottom: ${props.fixedStyle.bottom}px;
    `}
    ${props.fixedStyle.left && css`
      left: ${props.fixedStyle.left}px;
    `}
    ${props.fixedStyle.right && css`
      right: ${props.fixedStyle.right}px;
    `}

    ${props.fixedStyle.transformOrigin && css`
      ${AnimatedCard}{
        transform-origin: ${props.fixedStyle.transformOrigin};
      }
    `}
  `}
`;

const ToggleHeight = (allProps) => {

  const { style = {}, ...props } = allProps;

  const opacityProps = useSpring({
    opacity: props.show ? 1 : 0,
    display: props.show ? 'block' : 'none',
    config: { mass: 1, tension: 360, friction: 26 }
  });

  const scaleProps = useSpring({
    transform: props.show ? 'scale(1)' : 'scale(0.8)',
    config: { mass: 1, tension: 360, friction: 26 }
  });

  return (
    <AnimatedCard p0 elevation={2} {...props} style={{ overflow: 'hidden', ...opacityProps, ...scaleProps, ...style }}>
        {props.children}
    </AnimatedCard>
  );
};

ToggleHeight.propTypes = {
  show: PropTypes.any,
  children: PropTypes.node,
  measureRef: PropTypes.any
};

class Options extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      open: false,
      height: 0
    };
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.outsideClick, false);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.outsideClick, false);
  }

  render() {
    const { fixed, fixedStyle , triggerComponent:passedTriggerComponent, ...props } = this.props;
    const triggerComponent = passedTriggerComponent
                             ? passedTriggerComponent
                             : (<Button active={this.state.open ? true : false} type="action">
                                  <MdMoreVert />
                              </Button>);

    return (
      <OptionsWrap ref={ node => (this.node = node) }>
        <div style={{ display: 'inline-block' }} onClick={this.handleToggle}>
          { triggerComponent }
        </div>

        <ContainerWrap fixed={fixed} fixedStyle={fixedStyle}>
          <ToggleHeight {...props} show={ this.state.open ? 1 : 0 }>
              { this.props.children({
                close: (e, dontTriggerWhenClose) => this.close(e, dontTriggerWhenClose)
              }) }
          </ToggleHeight>
        </ContainerWrap>
      </OptionsWrap>
    );
  }

  handleToggle = () => {
    const { whenOpen = ()=>{}, whenClose = ()=>{} } = this.props;
    this.setState({ open: !this.state.open });
    this.state.open ? whenClose() : whenOpen();
  }

  close = (e, dontTriggerWhenClose) => {
    const { whenClose = ()=>{} } = this.props;
    this.setState({ open: false });
    whenClose(dontTriggerWhenClose);
  }

  // eslint-disable-next-line no-undef
  outsideClick = (e) => {
    const { whenClose = ()=>{} } = this.props;
    if (this.node.contains(e.target)) { return; }
    this.setState({ open: false });
    whenClose();
  }

  // eslint-disable-next-line no-undef
  setHeight = (contentRect) => {
    this.setState({
      height: contentRect.entry ? contentRect.entry.height : 0
    });
  }

}

Options.propTypes = {
  triggerComponent: PropTypes.node,
  children: PropTypes.func
};


const StyledHeader = styled.div`
  padding: 20px;
  padding-bottom:0;
`;
const Content = styled.div`
  padding: 0 20px;
  margin: 20px 0;
`;
const StyledFooter = styled.div`
  padding: 20px;
  padding-top: 0;
`;

const Header = (props)=>(
  <React.Fragment>
    <StyledHeader>
      { props.children }
    </StyledHeader>
    <Separator style={{margin:0, marginTop: '20px', marginBottom: '20px'}} />
  </React.Fragment>
);

const Footer = (props)=>(
  <React.Fragment>
    <Separator style={{margin:0, marginTop: '20px', marginBottom: '20px'}} />
    <StyledFooter>
      { props.children }
    </StyledFooter>
  </React.Fragment>
);

Options.Footer = Footer;
Options.Header = Header;
Options.Content = Content;

export default styled(Options)`
  min-width: 260px;
`;
