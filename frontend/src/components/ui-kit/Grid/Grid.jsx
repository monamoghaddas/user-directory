import { Row as AntRow, Col as AntCol } from 'antd'
import styled from 'styled-components'
import { mobileL } from '../../devices'

export const Row = styled(AntRow)``

export const Col = styled(AntCol)`
  ${mobileL`
    width:100%;
    `}
`
