import { Skeleton } from 'antd'
import styled, { css } from 'styled-components'

export default styled(Skeleton)`
  &&&&{
    ${props =>
      props.noTitle &&
      css`
        .ant-skeleton-title {
          display: none;
        }
        .ant-skeleton-paragraph {
          margin-top: 0;
        }
      `}

    ${props =>
      (props.list || props.table) &&
      css`
        .ant-skeleton-title {
          display: none;
        }
        .ant-skeleton-paragraph {
          margin-top: 0;
          li {
            height: 42px;
            border-radius: 6px;
            ${props.table &&
              css`
                height: 30px;
                border-radius: 0px;
              `}
            margin: 0;
            margin-bottom: 24px;
          }
        }
      `}

    ${props =>
      props.infocard &&
      css`
        .ant-skeleton-title {
          display: none;
        }
        .ant-skeleton-paragraph {
          margin-top: 0;
          li {
            margin-top: 0;
            border-radius: 6px;
            display: inline-block;
            margin-bottom: 10px;

            height: 100px;
            max-width: 230px;

            ${props.width &&
              css`
                max-width: ${props.width}px;
              `}

            ${props.height &&
              css`
                height: ${props.height}px;
              `}

          margin-right: 10px;
            ${props.gap &&
              css`
                margin-right: ${props.gap}px;
              `}
          }
        }
      `}
  }
`
