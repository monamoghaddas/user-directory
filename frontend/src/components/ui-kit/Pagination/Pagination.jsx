import { Pagination } from 'antd'
import styled from 'styled-components'

export default styled(Pagination)`
  margin-bottom:30px;
  .ant-select-focused{
    border-color:#ff6708;
  }
  .ant-pagination-item:hover,
  .ant-pagination-item-link:hover,
  .ant-select-selection.ant-select-selection--single:hover,
  .ant-select-selection:active,
  .ant-select-enabled,
  .ant-select-enabled:active,
  .ant-select-enabled:focused,
  .ant-select-open{
    border-color:#ff6708;
    svg{
      color:#ff6708;
    }
    a{
      color:#ff6708;
    }
  }

  .ant-select-open .ant-select-selection{
    border-color:#ff6708;
    svg{
      color:#ff6708;
    }
    a{
      color:#ff6708;
    }
  }

  .ant-select-selection:active{
    border-color:#ff6708 !important;
  }

  .ant-select-dropdown-menu-item-active{
    background-color: #ff67081a !important;
  }

  .ant-select-selection:hover{
    border-color:#ff6708;
    svg{
      color:#ff6708;
    }
    a{
      color:#ff6708;
    }
  }

  .ant-select-selection:focused,
  .ant-select-focused .ant-select-selection,
  .ant-select-selection:focus,
  .ant-select-selection:active{
    border-color:#ff6708 !important;
    svg{
      color:#ff6708 !important;
    }
    a{
      color:#ff6708;
    }
  }

  .ant-pagination-item-active{
    border-color:#ff6708;
    a{
      color:#ff6708;
    }
  }
`
