import Layout from './Layout'
import Grid from './Grid'
import Form from './Form'
import Input from './Input'
import Text from './Text'
import Button from './Button'
import Icon from './Icon'
import * as Theme from './Theme'
import Drawer from './Drawer'
import Loader from './Loader'
import Skeleton from './Skeleton'
import Card from './Card'
import Options from './Options'
import Pagination from './Pagination'

export default {
  Layout,
  Row: Grid.Row,
  Col: Grid.Col,
  Form,
  Input,
  Text,
  Button,
  Icon,
  Theme,
  Drawer,
  Loader,
  Skeleton,
  Card,
  Options,
  Pagination
}
