import React from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'
import { Layout as AntLayout } from 'antd'
import { mobileL } from '../../devices'

import 'antd/es/layout/style/css'

const {
  Header: AntHeader,
  Footer: AntFooter,
  Sider: AntSider2,
  Content: AntContent2
} = AntLayout

// eslint-disable-next-line no-unused-vars
const AntSider = ({ vCenter, vCenterAll, vCenterChild, stretch, ...rest }) => (
  <>
    <AntSider2 {...rest} />
  </>
)
AntSider.propTypes = {
  vCenter: PropTypes.any,
  vCenterAll: PropTypes.any,
  vCenterChild: PropTypes.any,
  stretch: PropTypes.any
}

// eslint-disable-next-line no-unused-vars
const AntContent = ({ vCenter, stretch, ...rest }) => (
  <>
    <AntContent2 {...rest} />
  </>
)
AntContent.propTypes = {
  vCenter: PropTypes.any,
  stretch: PropTypes.any
}

export const Layout = styled(AntLayout)`
  background-color: transparent;
  ${props => props.vCenter && css``}
`

export const Header = styled(AntHeader)`
  && {
    background-color: rgb(26, 96, 207);
    color: #fff;
    line-height: initial;
  }
`

export const Footer = styled(AntFooter)`
  background-color: #292929;
  color: #fff;
`

/**
 * Sider Component
 * Renders auto width if no width is provided
 */
const StyledSider = styled(AntSider)`
  background-color: transparent;
  color: inherit;

  ${props =>
    props.vCenter &&
    css`
      & {
        display: flex;
        align-items: center;
      }
    `}

  ${props =>
    props.vCenterChild &&
    css`
      .ant-layout-sider-children {
        display: flex;
        align-items: center;
        flex-grow: 1;
        justify-content: center;
      }
    `}

  ${props =>
    props.vCenterAll &&
    css`
      &,
      .ant-layout-sider-children {
        display: flex;
        align-items: center;
        flex-grow: 1;
      }
    `}

  ${props =>
    props.stretch &&
    css`
      display: flex;
      .ant-layout-sider-children {
        display: flex;
        width: 100%;
        flex-grow: 1;
      }
    `}
    `
export const Sider = ({ width, ...props }) => (
  <StyledSider {...props} width={width || 'auto'}>
    {props.children}
  </StyledSider>
)
Sider.propTypes = {
  children: PropTypes.node,
  width: PropTypes.any
}

export const Content = styled(AntContent)`
  &&&& {
    overflow-x: visible;
    background-color: inherit;
    ${props =>
      props.vCenter &&
      css`
        display: flex;
        align-items: center;
      `}

    ${props =>
      props.stretch &&
      css`
        display: flex;
      `}
  }
`

export const Page = styled(AntContent)`
  &&&& {
    margin: 0 60px;
    position: relative;
    ${mobileL`
      margin: 0 15px;
      `}
  }
`

/* Page Header */
export const PageHeader = styled.div`
  padding: 40px 0 30px 0;
  ${mobileL`
    padding: 60px 0 30px 0;
    `}
`
