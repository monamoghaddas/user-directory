import styled, { css } from 'styled-components'

export default styled.div`
  content: "";
  border-color: #eaeaea;
  border-width: 1px;
  border-top-style: solid;
  margin: 20px 0;

  ${props =>
    props.gap &&
    css`
      margin: ${props.gap}px 0;
    `}

  ${props =>
    props.left &&
    css`
      border-top-style: none;
      border-left-style: solid;
    `}

  ${props =>
    props.right &&
    css`
      border-top-style: none;
      border-right-style: solid;
    `}

  ${props =>
    props.bottom &&
    css`
      border-top-style: none;
      border-bottom-style: solid;
    `}

  ${props =>
    props.top &&
    css`
      border-top-style: solid;
    `}

  ${props =>
    props.bold &&
    css`
      border-width: 3px;
    `}

  ${props =>
    props.thick &&
    css`
      border-width: 5px;
    `}

`
