import * as Layouts from './layout'
import Separator from './seperator'

const { Layout } = Layouts
Layout.Header = Layouts.Header
Layout.Content = Layouts.Content
Layout.Sider = Layouts.Sider
Layout.Footer = Layouts.Footer
Layout.Separator = Separator
Layout.Page = Layouts.Page
Layout.PageHeader = Layouts.PageHeader

export default Layout
