import { css } from 'styled-components'

const colors = {
  gold: 'rgb(250,173,20)',
  red: 'rgb(230,41,41)',
  green: 'rgb(82,196,26)',
  magenta: 'rgb(235,47,150)',
  volcano: 'rgb(250,84,28)',
  blue: 'rgb(0,150,255)',
  geekblue: 'rgb(27, 101, 237)',
  orange: 'rgb(250,140,22)',
  lime: 'rgb(160,217,17)',
  cyan: 'rgb(27,185,154)',
  purple: 'rgb(114,46,209)',
  mmoser: '#ff6708'
}

const commonStyle = css`
    &&{
        margin-bottom: 0.5rem;
        margin-top: 0;
        /* All fonts to be 400 even headings */
        font-weight:400;

        ${props =>
          props.m0 &&
          css`
            margin: 0;
          `}

        ${props =>
          props.color &&
          css`
            color: ${colors[props.color] || props.color};
          `}

        ${props =>
          props.weight &&
          css`
            font-weight: ${props.weight};
          `}
    }
`

const titleStyle = css`
  ${commonStyle}
  font-size: 3rem;
`

const h1Style = css`
  ${commonStyle}
  font-size: 2.3rem;
`

const h2Style = css`
  ${commonStyle}
  font-size: 1.8rem;
`

const h3Style = css`
  ${commonStyle}
  font-size: 1.65rem;
`

const h4Style = css`
  ${commonStyle}
  font-size: 1.4rem;
`

const h5Style = css`
  ${commonStyle}
  font-size: 1.2rem;
`

const h6Style = css`
  ${commonStyle}
  font-size: 1rem;
  color:#ff6708;
  margin-right: 5px !important;
`

const pStyle = css`
  ${commonStyle}
`

export { titleStyle, h1Style, h2Style, h3Style, h4Style, h5Style, h6Style, pStyle }
