import PropTypes from 'prop-types' // eslint-disable-line no-unused-vars
import styled from 'styled-components'

import * as style from './text'

const Title = styled.h1`
  ${style.titleStyle}
`
const H1 = styled.h1`
  ${style.h1Style}
`
const H2 = styled.h2`
  ${style.h2Style}
`
const H3 = styled.h3`
  ${style.h3Style}
`
const H4 = styled.h4`
  ${style.h4Style}
`
const H5 = styled.h5`
  ${style.h5Style}
`
const H6 = styled.h6`
  ${style.h6Style}
`
const P = styled.p`
  ${style.pStyle}
`

const Text = P
Text.Title = Title
Text.H1 = H1
Text.H2 = H2
Text.H3 = H3
Text.H4 = H4
Text.H5 = H5
Text.H6 = H6
Text.P = P

export default Text
