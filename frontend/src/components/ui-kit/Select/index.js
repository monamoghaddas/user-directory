import React, { forwardRef } from 'react'
import styled from 'styled-components'
import StyledSelect from './Select'
import Material from '../Input/Material'

const MaterialSelect = (props, ref) => (
  <Material {...props} component={StyledSelect} ref={ref} />
)
const Select = styled(forwardRef(MaterialSelect))``
export default Select
