import React from 'react'
import styled, { css } from 'styled-components'
import { Select as AntSelect } from 'antd'

const { Option } = AntSelect

const StyledSelect = styled(AntSelect)`
  && {
    display: block;
    font-size: 1rem;

    .ant-select-selection {
      height: 40px;
    }

    .ant-select-selection__rendered {
      height: 100%;
      line-height: 40px;
    }

    ${props =>
      props.size &&
      css`
        ${props.size === 'large' &&
          css`
            .ant-select-selection {
              height: 55px;
            }
            .ant-select-selection__rendered {
              line-height: 55px;
            }
          `}
      `}

    .ant-select-search.ant-select-search--inline {
      height: inherit;
      margin-top: 0;
    }

    .ant-select-selection--multiple .ant-select-selection__rendered > ul > li {
      height: 32px;
      line-height: 32px;
      border-radius: 32px;

      ${props =>
        props.size &&
        css`
          ${props.size === 'large' &&
            css`
              height: 46px;
              line-height: 46px;
              border-radius: 46px;
              padding: 0 30px 0 20px;
            `}
        `}
    }

    .ant-select-selection--multiple .ant-select-selection__choice {
      background-color: #e9eeef;
      border: none;
    }

    .ant-select-selection--multiple .ant-select-selection__choice__remove {
      font-size: 14px;
      margin-top: 9px;

      ${props =>
        props.size &&
        css`
          ${props.size === 'large' &&
            css`
              margin-top: 15px;
              margin-right: 5px;
            `}
        `}
    }

    .ant-select-search__field {
      line-height: 44px;
      height: 100%;
      display: inline-block;
    }

    ${props =>
      props.material &&
      css`
        .ant-select-selection {
          height: 60px;
        }
        .ant-select-selection {
          height: 60px;
        }
        .ant-select-selection--multiple .ant-select-selection__rendered > ul > li {
          height: 50px;
          line-height: 50px;
          border-radius: 4px;
        }
        .ant-select-selection--multiple .ant-select-selection__choice__remove {
          margin-top: 18px;
        }
        .ant-select-selection__rendered {
          line-height: 60px;
        }
        .ant-select-selection-selected-value {
          margin-left: 10px;
        }
      `}
  }
`

const Select = ({ options, ...rest }, ref) => (
  <StyledSelect {...rest}>
    {options &&
        options.map(
          (option, i) =>
            <Option
              key={option.value}
              value={option.value}
              id={i}>
                {option.name}
              </Option>
            )
    }
  </StyledSelect>
)

export default styled(Select)``
