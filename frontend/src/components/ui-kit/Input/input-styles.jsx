import { css } from 'styled-components'

export default css`
  && {
    .ant-input-prefix {
      margin-left: 2px;

      svg {
        font-size: 1.2rem;
      }
    }

    .ant-input {
      min-height: auto;
    }
  }
`
