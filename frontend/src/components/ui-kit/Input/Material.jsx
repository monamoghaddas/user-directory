import React, { useState } from 'react'
import styled, { css } from 'styled-components'

import Input from './input'
import Password from './password'
import Select from '../Select/Select'

const Placeholder = styled.span`
  position: absolute;
  display: none;
  top: 50%;
  left: 12px;
  z-index: 2;
  transform: translatey(-50%);
  color: #ccc;
  pointer-events: none;
  transition: all 0.25s ease-in-out;
  font-size: 17px;

  ${props =>
    props.focused &&
    css`
      top: 15px;
      font-size: 11px;
      color: rgba(0, 0, 0, 0.8);
    `}
`

const MaterialInput = styled.div`
  .ant-input {
    height: 40px;
  }

  .ant-input-lg {
    height: 55px;
  }

  .ant-input-number {
    height: 40px;
    input {
      height: 40px;
    }
  }

  .ant-input-number-lg {
    height: 55px;
    input {
      height: 55px;
    }
  }

  ${props =>
    props.material &&
    css`

      position: relative;
      background-color: #fff;
      border: 1px solid #d9d9d9;
      border-radius: 4px;
      padding-top: 0;

      ${Placeholder}{
        display: block;
      }

      ${props =>
        props.focused &&
        css`
          border-color: #40a9ff;
          box-shadow: 0 0 0 2px rgba(24, 144, 255, 0.2);

          ${Placeholder} {
            top: 15px;
            font-size: 11px;
          }
        `}

      ${Select}{
          .ant-select-selection__rendered{
            line-height: 28px;
          }
          .ant-select-selection__rendered > ul > li{
            height: 26px !important;
            line-height: 26px !important;
          }
      }

      ${Input},${Password},${Select}{
        height: 100%;
        padding-bottom: 5px;
        padding-left: 10px;
        padding-top: 20px;
        font-size: 17px;
        border: none;
        outline: none;
        box-shadow: none;
        width: 100%;
        color: rgba(0, 0, 0, 0.8);

        .ant-select-arrow{
          top: 10px;
        }

        .ant-select-selection, .ant-select-selection__rendered{
          border: none;
          background-color: transparent;
          box-shadow: none;

          &:focus, &:active{
            box-shadow: none;
          }
        }
      }

      input:-webkit-autofill,
      input:-webkit-autofill:hover,
      input:-webkit-autofill:focus,
      input:-webkit-autofill:active  {
          -webkit-box-shadow: 0 0 0 30px white inset !important;
      }

      height: 55px;
    `}
`

export default props => {
  const {
    placeholder,
    material,
    component,
    onChange = () => {},
    onFocus = () => {},
    onBlur = () => {},
    style = {},
    ...rest
  } = props
  const [focused, useFocused] = useState(false)

  const { value, defaultValue } = rest
  const [val, useVal] = useState(value || defaultValue)
  const Component = component

  return (
    <MaterialInput focused={focused} material={material} style={style}>
      <Placeholder focused={focused || (val && val.length > 0)}>
        {placeholder || ''}
      </Placeholder>
      <Component
        onChange={e => {
          useVal(e.target ? e.target.value : e)
          onChange(e)
        }}
        onFocus={e => {
          useFocused(true)
          onFocus(e)
        }}
        onBlur={e => {
          useFocused(false)
          onBlur(e)
        }}
        placeholder={material ? '' : placeholder}
        {...rest}
      />
    </MaterialInput>
  )
}
