import React, { forwardRef } from 'react'
import styled from 'styled-components'
import Material from './Material'
import StyledInput from './input'
import StyledPassword from './password'

const { Search, TextArea } = StyledInput

const MaterialInput = (props, ref) => (
  <Material {...props} component={StyledInput} ref={ref} />
)
const MaterialPassword = (props, ref) => (
  <Material {...props} component={StyledPassword} ref={ref} />
)

const Input = styled(forwardRef(MaterialInput))``
Input.Password = styled(forwardRef(MaterialPassword))``
Input.Search = styled(Search)``
Input.TextArea = styled(TextArea)``

export default Input
