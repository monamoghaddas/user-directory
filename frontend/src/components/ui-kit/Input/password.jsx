import styled from 'styled-components'

import { Input } from 'antd'
import inputStyles from './input-styles'

export default styled(Input.Password)`
  ${inputStyles}
`
