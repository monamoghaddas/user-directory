import React from 'react'
import styled, { css } from 'styled-components'
import Icon from '../Icon'

const LoaderWrap = styled.div`
  display: inline-block;
  border-radius: 30px;
  background-color: #0096ff;
  box-shadow: 0 2px 6px rgba(0, 150, 255, 0.3);
  left: 50%;
  top: 50%;
  padding: 20px;
  z-index: 2;
  transition: all ease-in-out 0.4s;

  /* Position */
  position: absolute;
  ${props =>
    props.fixed &&
    css`
      position: fixed;
    `}

  /* Render options */
  transform: translate(-50% ,-50%);

  ${props =>
    typeof props.show !== undefined &&
    css`
      transform: translate(-50%, -50%) scale(0.9, 0.9);
      opacity: 0;
      visibility: hidden;

      ${props.show &&
        css`
          transform: translate(-50%, -50%) scale(1, 1);
          opacity: 1;
          visibility: visible;
        `}
    `}
`

const Loader = props => (
  <LoaderWrap {...props}>
    <Icon type='loading' style={{ fontSize: 30, color: '#fff' }} />
  </LoaderWrap>
)

export default styled(Loader)``
