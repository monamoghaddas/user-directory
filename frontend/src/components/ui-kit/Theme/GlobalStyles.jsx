import { createGlobalStyle } from 'styled-components'

export default createGlobalStyle`
    &&{
        body{
            font-size: 13px;
        }
        *{
            &.m-0{
                margin: 0;
            }
        }
        .ant-card{
            color: inherit;
        }
        .ant-calendar-range-picker-separator{
            line-height: 2.3;
        }
        .ant-input{
            height: 40px;
        }

        .ant-input-affix-wrapper .ant-input{
            min-height: unset;
        }

        .ant-layout{
            &&{
                background-color: inherit;
            }
        }
        .ant-layout-sider{
            &&{
                background-color: transparent;
            }
        }

        .ant-menu-item > a{
            &&{
                color: inherit;
                &:hover{
                    color: inherit;
                }
            }
        }
    }
`
