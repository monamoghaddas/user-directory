import { Drawer as AntDrawer } from 'antd'
import styled, { css } from 'styled-components'

const widths = {
  sm: '256px',
  md: '500px',
  lg: '700px',
  xl: '1000px',
  xxl: '1200px'
}

export default styled(AntDrawer)`
  && {
    .ant-drawer-wrapper-body {
      max-width: 100%;

      ${props =>
        props.width &&
        css`
          ${widths[props.width] &&
            css`
              width: ${widths[props.width]};
            `}
        `}
    }
    .ant-drawer-content-wrapper {
      max-width: 500px;
      width: 100%;
    }
    .ant-drawer-header,
    .ant-drawer-body {
      padding: 30px 40px;
    }
    .ant-drawer-body {
      padding-top: 20px;
    }

    .ant-drawer-close {
      top: 10px;
      right: 16px;

      i {
        padding: 10px;
        background-color: rgba(250, 84, 28, 0.1);
        border-radius: 40px;
        font-size: 22px;
        color: rgb(250, 84, 28);

        &:hover {
          background-color: rgba(250, 84, 28, 1);
          color: #fff;
        }
      }
    }
  }
`
