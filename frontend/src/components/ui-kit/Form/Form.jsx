import { Form } from 'antd'
import styled from 'styled-components'

export default styled(Form)`
  .ant-form-item-control {
    line-height: initial;
    margin-bottom: 10px;
  }
`
