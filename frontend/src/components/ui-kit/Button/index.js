import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import StyledButton from './Button'
import Icon from '../Icon'

const LoaderWrap = styled.span`
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
`

const Button = props => {
  const { children, disabled, loading, type, htmlType, loadingText, ...rest } = props

  const submitType = {}
  if (htmlType) {
    submitType.type = htmlType
  }

  return (
    <StyledButton
      {...rest}
      styletype={type}
      {...submitType}
      disabled={loading ? true : disabled}
    >
      {loadingText === '' ? (
        <>
          {loading ? (
            <LoaderWrap>
              <Icon style={{ verticalAlign: 'middle' }} type='loading' />
            </LoaderWrap>
          ) : null}
          <span style={{ visibility: loading ? 'hidden' : 'visible' }}>{children}</span>
        </>
      ) : (
        <>
          {loading ? <Icon style={{ verticalAlign: 'middle' }} type='loading' /> : null}
          {loading ? (
            <>{typeof loadingText === 'undefined' ? ` ${children}` : ` ${loadingText}`}</>
          ) : (
            children
          )}
        </>
      )}
    </StyledButton>
  )
}

Button.propTypes = {
  children: PropTypes.oneOfType([PropTypes.object, PropTypes.string, PropTypes.element]),
  disabled: PropTypes.bool,
  loading: PropTypes.string,
  type: PropTypes.string,
  htmlType: PropTypes.string,
  loadingText: PropTypes.string
}

export default Button
