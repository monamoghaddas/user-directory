import PropstyleTypes from 'prop-types' // eslint-disable-line no-unused-vars
import { opacify, darken, adjustHue } from 'polished'
import styled, { css } from 'styled-components'
import theme from '../../utils/theme'
import * as helpers from '../../utils/helpers'

const colors = {
  gold: 'rgb(250,173,20)',
  red: 'rgb(230,41,41)',
  green: 'rgb(82,196,26)',
  magenta: 'rgb(235,47,150)',
  volcano: 'rgb(250,84,28)',
  blue: 'rgb(0,150,255)',
  geekblue: 'rgb(27, 101, 237)',
  orange: 'rgb(250,140,22)',
  lime: 'rgb(160,217,17)',
  cyan: 'rgb(27,185,154)',
  purple: 'rgb(114,46,209)',
  mmoser: '#ff6708'
}

const Button = styled.a`
    position: relative;
    border-radius: 3px;
    display: inline-block;
    box-sizing: border-box;
    font-size: 0.95rem;
    cursor: pointer;
    color: ${theme.bg.primary};
    background-color: ${opacify(-0.9, colors.blue)};
    font-weight: 600;
    outline: none;
    border: none;

    user-select: none;
    text-align: center;
    transition: all 0.12s ease-in-out;

    padding: 9px 25px;
    ${props =>
      props.styleType === 'list' &&
      css`
        padding: 8px 20px;
      `}

    svg{
        font-size: 1.25rem;
        vertical-align: middle;
    }

    /* Same active and hover styles */
    ${props =>
      props.active &&
      props.active === true &&
      css`
        background-color: ${theme.bg.primaryD2} !important;
        color: #fff !important;
        box-shadow: 0px 0px 0px 1px ${theme.bg.primaryD2} !important;
        position: relative !important;
      `}
    :hover{
        background-color: ${colors.mmoser};
        color: #fff;
        box-shadow:0px 0px 0px 1px ${colors.mmoser};
        position: relative;
    }

    :disabled{
      cursor: not-allowed;
        pointer-events: none;
        color: #c0c0c0;
        background-color: #e8e8e8;
    }

    :active{
        background-color: ${colors.mmoser};
    }

    /**
    *   IF BACKGROUND COLOR IS SPECIFIED FOR DEFAULT BUTTON
    */
   ${props =>
     props.background &&
     css`
       ${colors[props.background]
         ? css`
             background-color: ${opacify(-0.9, colors[props.background])};
             color: ${colors[props.background]};

             /* Same active and Hover styles */
             ${props.active &&
                 props.active === true &&
                 css`
                   background-color: ${colors[props.background]} !important;
                   color: #fff !important;
                   box-shadow: 0px 0px 0px 3px ${colors[props.background]} !important;
                 `}
               :hover {
               background-color: ${colors[props.background]};
               color: #fff;
               box-shadow: 0px 0px 0px 3px ${colors[props.background]};
             }

             :active {
               background-color: ${adjustHue(-3, darken(0.08, colors[props.background]))};
               box-shadow: 0px 0px 0px 1px
                 ${adjustHue(-3, darken(0.08, colors[props.background]))};
               color: #fff;
             }
           `
         : css``}
     `}

    ${props =>
      props.full &&
      css`
        display: block;
      `}

    ${props =>
      props.size &&
      css`
        ${props.size === 'sm' &&
          css`
            padding: 6px 18px;
            background-color:${colors.mmoser};
            color:#fff;
            font-size: 0.9rem;
            svg {
              font-size: 1.2rem;
            }
            ${props.styleType === 'list' &&
              css`
                padding: 6px 10px;
              `}
          `}
        ${props.size === 'lg' &&
          css`
            padding: 15px 30px;
            font-size: 1.2rem;
            svg {
              font-size: 2.4rem;
            }
            ${props.styleType === 'list' &&
              css`
                padding: 15px 20px;
              `}
          `}
      `}

    ${props =>
      props.styleType &&
      css`

        ${props.styleType === 'primary' &&
          css`
            background-color: ${theme.bg.primary};
            color: ${theme.text.primary};
            border: none;
            box-shadow: 0 6px 15px ${helpers.rgba(theme.bg.primary, 0.3)};

            /* Same active and Hover styles */
            ${props.active &&
                props.active === true &&
                css`
                  background-color: ${theme.bg.primaryD1} !important;
                  color: ${theme.text.primaryD1} !important;
                  border: none !important;
                  box-shadow: 0 6px 15px ${helpers.rgba(theme.bg.primaryD1, 0.3)},
                    0px 0px 0px 1px ${theme.bg.primaryD1} !important;
                `}
              :hover {
              background-color: ${theme.bg.primaryD1};
              color: ${theme.text.primaryD1};
              border: none;
              box-shadow: 0 6px 15px ${helpers.rgba(theme.bg.primaryD1, 0.3)},
                0px 0px 0px 1px ${theme.bg.primaryD1};
            }

            &[disabled] {
              box-shadow: none;
              color: #fff;
              opacity: 0.5;
            }

            :active {
              background-color: ${theme.bg.primaryD2};
              box-shadow: 0 3px 10px ${helpers.rgba(theme.bg.primaryD2, 0.3)};
            }

            /* IF CUSTOM COLORS PROVIDED FOR PRIMARY BUTTON */
            ${props.background &&
              css`
                ${colors[props.background]
                  ? css`
                      box-shadow: 0 6px 15px ${opacify(-0.5, colors[props.background])};
                      color: #fff;
                      background-color: ${colors[props.background]};

                      /* Same active and Hover styles */
                      ${props.active &&
                          props.active === true &&
                          css`
                            background-color: ${adjustHue(
                              -3,
                              darken(0.05, colors[props.background])
                            )} !important;
                            box-shadow: 0 6px 15px
                                ${opacify(
                                  -0.5,
                                  adjustHue(-3, darken(0.05, colors[props.background]))
                                )},
                              0 0 0 3px
                                ${adjustHue(-3, darken(0.05, colors[props.background]))} !important;
                          `}
                        :hover {
                        background-color: ${adjustHue(
                          -3,
                          darken(0.05, colors[props.background])
                        )};
                        box-shadow: 0 6px 15px
                            ${opacify(
                              -0.5,
                              adjustHue(-3, darken(0.05, colors[props.background]))
                            )},
                          0 0 0 3px
                            ${adjustHue(-3, darken(0.05, colors[props.background]))};
                      }

                      :active {
                        background-color: ${adjustHue(
                          -3,
                          darken(0.08, colors[props.background])
                        )};
                        box-shadow: 0 3px 10px
                          ${opacify(
                            -0.5,
                            adjustHue(-3, darken(0.08, colors[props.background]))
                          )};
                      }
                    `
                  : css``}
              `}
          `}

        ${(props.styleType === 'action' || props.styleType === 'minimal') &&
          css`
            border-radius: 3px;
            color: #595959;
            background-color: #fff;
            box-shadow: 0px 0px 0px 1px rgba(0, 0, 0, 0.13);

            ${props.background &&
              css`
                ${colors[props.background] &&
                  css`
                    background-color: ${opacify(-0.9, colors[props.background])};
                    box-shadow: 0px 0px 0px 1px ${colors[props.background]};
                    color: ${colors[props.background]};

                    /* Same active and hover */
                    ${props.active &&
                        props.active === true &&
                        css`
                          background-color: ${colors[props.background]} !important;
                          color: #fff !important;
                        `}
                      :hover {
                      background-color: ${colors[props.background]};
                      color: #fff;
                    }
                    :active {
                      background-color: ${adjustHue(
                        -3,
                        darken(0.08, colors[props.background])
                      )};
                      box-shadow: 0px 0px 0px 1px
                        ${adjustHue(-3, darken(0.08, colors[props.background]))};
                    }
                  `}
              `}
            padding: 3px;

            ${props.styleType === 'minimal' &&
              css`
                box-shadow: none;
                background-color: transparent;
              `}

            width: 32px;
            height: 32px;
            text-align: center;
            svg {
              margin-top: 1px;
            }

            ${props.size === 'sm' &&
              css`
                padding: 3px;
                border-radius: 3px;
                width: 25px;
                height: 25px;
                svg {
                  margin-top: 0px;
                }
              `}

            ${props.size === 'lg' &&
              css`
                padding: 5px;

                border-radius: 3px;
                width: 55px;
                height: 55px;
                svg {
                  margin-top: 3px;
                }
              `}
          `}


        ${(props.styleType === 'flat' || props.styleType === 'list') &&
          css`
            border-radius: 3px;
            box-shadow: none;
            background-color: #efefef;

            ${props.styleType === 'list' &&
              css`
                background-color: transparent;
                color: inherit;
                text-align: left;
              `}

            ${props.background &&
              css`
                ${colors[props.background]
                  ? css`
                    color: ${colors[props.background]};
                    ${props.styleType === 'flat' &&
                      css`
                        background-color: ${opacify(-0.9, colors[props.background])};
                      `}

                    /* Same active and hover */
                    ${props.active &&
                      props.active === true &&
                      css`
                        background-color: ${colors[props.background]} !important;
                        box-shadow: 0px 0px 0px 1px ${colors[props.background]} !important;
                        color: #fff !important;
                      `}

                    :hover{
                        background-color: ${colors[props.background]};
                        box-shadow: 0px 0px 0px 1px ${colors[props.background]};
                        color: #fff;
                    }

                    :active{
                        background-color: ${adjustHue(
                          -3,
                          darken(0.08, colors[props.background])
                        )};
                        box-shadow:0px 0px 0px 1px ${adjustHue(
                          -3,
                          darken(0.08, colors[props.background])
                        )};
                    }
                    `
                  : css``}
              `}
          `}

    `}

`

export default Button
