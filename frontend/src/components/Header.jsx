import React from 'react'
import logo from '../assets/logo.svg'
import './Header.css'

const Header = () => {
  return (
    <header>
      <div className='nav'>
        <input type='checkbox' id='nav-check' />
        <div className='nav-header'>
          <div className='nav-title'>
            <img src={logo} />
          </div>
        </div>
        <div className='nav-btn'>
          <label htmlFor='nav-check'>
            <span></span>
            <span></span>
            <span></span>
          </label>
        </div>

        <div className='nav-links'>
          <a href='/logout'>Logout</a>
        </div>
      </div>
    </header>

  )
}

export default Header
