import { put, call, takeEvery } from 'redux-saga/effects'
import { toast } from 'react-toastify'
import * as actionType from '../global/actions'
import * as action from '../actions/app'
import apiCalls from '../global/Api'
import { setItem } from '../utils'

/** *************************** Subroutines *********************************** */
function* userLogin(option) {
  const { email, password } = option
  yield put(action.userLogin.request())
  try {
    const { response, error } = yield call(apiCalls.userLogin, { email, password })
    if (response) {
      setItem('token', response.token)
      yield put(action.userLogin.success(option, response))
    } else {
      toast.error(error)
      yield put(action.userLogin.failure(option, error))
    }
  } catch (error) {
    toast.error(error)
    yield put(action.userLogin.failure(error))
  }
}

/** *************************************************************************** */
/** ***************************** WATCHERS ************************************ */
/** *************************************************************************** */
export default function* watchJob() {
  yield takeEvery(actionType.APP_LOAD_USER_LOGIN, userLogin)
}
