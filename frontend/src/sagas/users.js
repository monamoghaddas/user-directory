import { put, call, takeEvery } from 'redux-saga/effects'
import { toast } from 'react-toastify'
import * as actionType from '../global/actions'
import * as action from '../actions/users'
import apiCalls from '../global/Api'

/** *************************** Subroutines *********************************** */
function* users() {
  yield put(action.loadUsers.loadUsersRequest())
  try {
    const { response, error } = yield call(apiCalls.users)
    if (response) {
      yield put(action.loadUsers.loadUsersSuccess(response))
    } else {
      toast.error(error)
      yield put(action.loadUsers.failure(error))
    }
  } catch (error) {
    toast.error(error)
    yield put(action.loadUsers.failure(error))
  }
}

function* user({ id }) {
  yield put(action.loadUser.loadUserRequest())
  try {
    const { response, error } = yield call(apiCalls.user, id)
    if (response) {
      yield put(action.loadUser.loadUserSuccess(response))
    } else {
      toast.error(error)
      yield put(action.loadUser.failure(error))
    }
  } catch (error) {
    toast.error(error)
    yield put(action.loadUser.failure(error))
  }
}

function* createUser(options) {
  yield put(action.createUser.createUserRequest())
  try {
    const { response, error } = yield call(apiCalls.createUser, options)
    if (response) {
      toast.success('User Created successfully.')
      yield put(action.createUser.createUserSuccess(response))
    } else {
      toast.error(error)
      yield put(action.createUser.failure(error))
    }
  } catch (error) {
    toast.error(error)
    yield put(action.loadUser.failure(error))
  }
}

function* updateUser(options) {
  const { id, values } = options
  yield put(action.updateUser.updateUserRequest())
  try {
    const { response, error } = yield call(apiCalls.updateUser, id, values)
    if (response) {
      toast.success('User Updated successfully.')
      yield put(action.updateUser.updateUserSuccess(response))
    } else {
      toast.error(error)
      yield put(action.updateUser.failure(error))
    }
  } catch (error) {
    toast.error(error)
    yield put(action.loadUser.failure(error))
  }
}

function* deleteUser(options) {
  const { id } = options
  yield put(action.deleteUser.deleteUserRequest())
  try {
    const { response, error } = yield call(apiCalls.deleteUser, id)
    if (response) {
      toast.success('User Deleted successfully.')
      yield put(action.deleteUser.deleteUserSuccess(response))
    } else {
      toast.error(error)
      yield put(action.deleteUser.failure(error))
    }
  } catch (error) {
    toast.error(error)
    yield put(action.loadUser.failure(error))
  }
}

/** *************************************************************************** */
/** ***************************** WATCHERS ************************************ */
/** *************************************************************************** */
export default function* watchJob() {
  yield takeEvery(actionType.USERS_LOAD, users)
  yield takeEvery(actionType.USER_LOAD, user)
  yield takeEvery(actionType.USER_CREATE, createUser)
  yield takeEvery(actionType.USER_UPDATE, updateUser)
  yield takeEvery(actionType.USER_DELETE, deleteUser)
}
