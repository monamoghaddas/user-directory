class BadRequest extends Error {
  constructor (message) {
    super(message)
    this.type = 'BadRequest'
    this.statusCode = 400
  }
}
class NotAuthenticated extends Error {
  constructor (message) {
    super(message)
    this.type = 'NotAuthenticated'
    this.statusCode = 401
  }
}
class NotFound extends Error {
  constructor (message) {
    super(message)
    this.type = 'NotFound'
    this.statusCode = 404
  }
}
class ServerError extends Error {
  constructor (message) {
    super(message)
    this.type = 'ServerError'
    this.statusCode = 500
  }
}

module.exports = { BadRequest, NotAuthenticated, NotFound, ServerError }
