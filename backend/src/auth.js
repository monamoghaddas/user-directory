const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const crypto = require('crypto')
const errors = require('./errors')

const AUTH_SECRET = crypto.randomBytes(64).toString('hex')

function hashPassword (password) {
  return bcrypt.hash(password, bcrypt.genSaltSync(8))
}

function validatePassword (password, hash) {
  return bcrypt.compare(password, hash)
}

function generateToken (user) {
  return new Promise((resolve, reject) => {
    jwt.sign({
      sub: user.id,
      email: user.email,
      loginTime: Date.now()
    }, AUTH_SECRET, (err, res) => {
      if (err) return reject(err)
      resolve(res)
    })
  })
}

function validateToken (token) {
  return new Promise((resolve, reject) => {
    jwt.verify(token, AUTH_SECRET, (err, res) => {
      if (err) return reject(err)
      res.userId = res.sub
      delete res.sub
      console.log('res', res)
      return resolve(res)
    })
  })
}

const AUTH_HEADER_PREFIX = 'Bearer '
function authenticate (req, res, next) {
  const authHeader = req.headers.authorization
  if (!authHeader || authHeader.indexOf(AUTH_HEADER_PREFIX) !== 0) {
    return next(new errors.NotAuthenticated('No valid authorization header present'))
  }

  const token = authHeader.substr(AUTH_HEADER_PREFIX.length)
  validateToken(token)
    .then(result => {
      req.auth = result
      next()
    })
    .catch(() => {
      next(new errors.NotAuthenticated('Invalid access token'))
    })
}

module.exports = {
  hashPassword,
  validatePassword,
  generateToken,
  validateToken,
  authenticate
}
