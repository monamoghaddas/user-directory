const express = require('express')
const bodyParser = require("body-parser")
const db = require('./db')
const init = require('./init')
const routes = require('./routes')
const cors = require('cors')

module.exports = async function App () {
  const app = express()

  // App setup
  app.use(bodyParser.urlencoded({ extended: true }))
  app.use(bodyParser.json())

  // Adding CORS
  app.use(cors())

  // Setup services
  await db(app)
  await init(app)
  await routes(app)

  return app
}
